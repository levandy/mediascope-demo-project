package net.mediascope.demo.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Collection;

import static java.util.Arrays.asList;
import static net.mediascope.demo.util.StatisticsCalculator.calculateSiteCount;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class StatisticsCalculatorTest {

    private BigDecimal reach;
    private Long holdingCount;
    private Long result;

    public StatisticsCalculatorTest(BigDecimal reach, Long holdingCount, Long result) {
        this.reach = reach;
        this.holdingCount = holdingCount;
        this.result = result;
    }

    @Parameterized.Parameters(name = "{index}: Test - reach={0}, holdingCount={1}, expected={2} ")
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{
                {new BigDecimal(1), 1000L, 10L},
                {new BigDecimal(10), 1000L, 100L},
                {new BigDecimal(10), 1667L, 166L},
                {new BigDecimal(15), 5000L, 750L},
                {new BigDecimal(20), 7777L, 1555L},
                {new BigDecimal(100), 1000L, 1000L},
                {new BigDecimal(0), 1000L, 0L}
        };
        return asList(data);
    }

    @Test
    public void calculateSiteCountTest() {
        assertEquals(result, calculateSiteCount(reach, holdingCount));
    }

}
