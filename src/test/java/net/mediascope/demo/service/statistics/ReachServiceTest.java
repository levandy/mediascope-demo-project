package net.mediascope.demo.service.statistics;

import net.mediascope.demo.dao.statistics.impl.ReachDaoImpl;
import net.mediascope.demo.dto.statistics.SiteReachDto;
import net.mediascope.demo.service.statistics.impl.ReachServiceImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static net.mediascope.demo.DataFactory.siteReachDetails1;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(SpringRunner.class)
public class ReachServiceTest {

    @Mock
    private ReachDaoImpl reachDao;
    @InjectMocks
    private ReachServiceImpl reachService;

    @Before
    public void setUp() {
        initMocks(this);
    }

    @Test
    public void nullDetailsReturnedTest() {
        when(reachDao.getAllSiteReachDetails()).thenReturn(null);
        List<SiteReachDto> actual = reachService.getDetails();
        assertEquals(0, actual.size());
    }

    @Test
    public void emptyDetailsReturnedTest() {
        when(reachDao.getAllSiteReachDetails()).thenReturn(emptyList());
        List<SiteReachDto> actual = reachService.getDetails();
        assertEquals(0, actual.size());
    }

    @Test
    public void oneSiteIdInOneDetailsReturnedTest() {
        when(reachDao.getAllSiteReachDetails()).thenReturn(singletonList(siteReachDetails1(1L, 2L)));
        List<SiteReachDto> actual = reachService.getDetails();
        assertEquals(1, actual.size());
        assertEquals(100L, (long) actual.get(0).getReach());
        assertEquals(1L, (long) actual.get(0).getSiteId());
    }

    @Test
    public void oneSiteIdInTwoDetailsReturnedTest() {
        when(reachDao.getAllSiteReachDetails()).thenReturn(
                asList(
                        siteReachDetails1(3L, 4L),
                        siteReachDetails1(3L, 5L)
                )
        );
        List<SiteReachDto> actual = reachService.getDetails();
        assertEquals(1, actual.size());
        assertEquals(200L, (long) actual.get(0).getReach());
        assertEquals(3L, (long) actual.get(0).getSiteId());
    }

    @Test
    public void twoSiteIdsInThreeDetailsReturnedTest() {
        when(reachDao.getAllSiteReachDetails()).thenReturn(
                asList(
                        siteReachDetails1(6L, 8L),
                        siteReachDetails1(7L, 9L),
                        siteReachDetails1(6L, 10L)
                )
        );
        List<SiteReachDto> actual = reachService.getDetails();
        assertEquals(2, actual.size());
        assertThat(actual.stream().map(SiteReachDto::getReach).collect(toList()), containsInAnyOrder(200L, 100L));
        assertThat(actual.stream().map(SiteReachDto::getSiteId).collect(toList()), containsInAnyOrder(6L, 7L));
    }

}
