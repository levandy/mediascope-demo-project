package net.mediascope.demo;

import net.mediascope.demo.model.statistics.SiteReachDetails;

import java.math.BigDecimal;

public class DataFactory {

    public static SiteReachDetails siteReachDetails1(Long siteId, Long holdingId) {
        SiteReachDetails details = new SiteReachDetails();
        details.setSiteId(siteId);
        details.setHoldingId(holdingId);
        details.setHoldingCount(2000L);
        details.setReach(new BigDecimal(5L));
        return details;
    }
}
