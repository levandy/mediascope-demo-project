package net.mediascope.demo.entity.statistics;

import java.math.BigDecimal;

public class ReachEntity {
    /**
     * Идентификатор записи.
     */
    private Long id;
    /**
     * Идентификатор холдинга.
     */
    private Long holdingId;
    /**
     * Идентификатор сайта.
     */
    private Long siteId;
    /**
     * Охват населения (%).
     */
    private BigDecimal reach;

    public ReachEntity() {
    }

    public ReachEntity(Long id, Long holdingId, Long siteId, BigDecimal reach) {
        this.id = id;
        this.holdingId = holdingId;
        this.siteId = siteId;
        this.reach = reach;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHoldingId() {
        return holdingId;
    }

    public void setHoldingId(Long holdingId) {
        this.holdingId = holdingId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public BigDecimal getReach() {
        return reach;
    }

    public void setReach(BigDecimal reach) {
        this.reach = reach;
    }
}
