package net.mediascope.demo.entity.statistics;

import java.io.Serializable;

public class HoldingCounterEntity implements Serializable {
    /**
     * Идентификатор записи.
     */
    private Long id;
    /**
     * Идентификатор холдинга.
     */
    private Long holdingId;
    /**
     * Количество посещений холдинга.
     */
    private Long count;

    public HoldingCounterEntity() {
    }

    public HoldingCounterEntity(Long id, Long holdingId, Long count) {
        this.id = id;
        this.holdingId = holdingId;
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getHoldingId() {
        return holdingId;
    }

    public void setHoldingId(Long holdingId) {
        this.holdingId = holdingId;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
