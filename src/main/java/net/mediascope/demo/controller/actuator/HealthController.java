package net.mediascope.demo.controller.actuator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.HealthEndpoint;
import org.springframework.stereotype.Controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Controller
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class HealthController {

    private HealthEndpoint health;

    @Autowired
    public HealthController(HealthEndpoint health) {
        this.health = health;
    }

    @GET
    @Path("/health")
    public Response getHealth() {
        return Response.ok().entity(health.health()).build();
    }
}
