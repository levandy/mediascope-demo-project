package net.mediascope.demo.controller.statistics;

import net.mediascope.demo.dto.statistics.SiteReachDto;
import net.mediascope.demo.service.statistics.ReachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static java.util.Comparator.comparingLong;

@Controller
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ReachController {

    private ReachService reachService;

    @Autowired
    public ReachController(ReachService reachService) {
        this.reachService = reachService;
    }

    @GET
    @Path("/reach")
    public Response getReach() {
        List<SiteReachDto> dtos = reachService.getDetails();
        dtos.sort(comparingLong(SiteReachDto::getSiteId));
        return Response.ok(dtos).build();
    }
}
