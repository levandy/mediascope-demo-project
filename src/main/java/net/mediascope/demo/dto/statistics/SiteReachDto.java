package net.mediascope.demo.dto.statistics;

public class SiteReachDto {
    /**
     * Идентификатор сайта.
     */
    private Long siteId;
    /**
     * Охват населения.
     */
    private Long reach;

    public SiteReachDto() {
    }

    public SiteReachDto(Long siteId, Long reach) {
        this.siteId = siteId;
        this.reach = reach;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getReach() {
        return reach;
    }

    public void setReach(Long reach) {
        this.reach = reach;
    }
}
