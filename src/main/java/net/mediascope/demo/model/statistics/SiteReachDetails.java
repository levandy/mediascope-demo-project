package net.mediascope.demo.model.statistics;

import java.math.BigDecimal;

public class SiteReachDetails {
    /**
     * Идентификатор холдинга.
     */
    private Long holdingId;
    /**
     * Идентификатор сайта.
     */
    private Long siteId;
    /**
     * Количество посещений холдинга.
     */
    private Long holdingCount;
    /**
     * Количество посещений сайта.
     */
    private Long siteCount;
    /**
     * Охват населения (%).
     */
    private BigDecimal reach;

    public SiteReachDetails() {
    }

    public SiteReachDetails(Long holdingId, Long siteId, Long holdingCount,
                            Long siteCount, BigDecimal reach) {
        this.holdingId = holdingId;
        this.siteId = siteId;
        this.holdingCount = holdingCount;
        this.siteCount = siteCount;
        this.reach = reach;
    }

    public Long getHoldingId() {
        return holdingId;
    }

    public void setHoldingId(Long holdingId) {
        this.holdingId = holdingId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Long getHoldingCount() {
        return holdingCount;
    }

    public void setHoldingCount(Long holdingCount) {
        this.holdingCount = holdingCount;
    }

    public Long getSiteCount() {
        return siteCount;
    }

    public void setSiteCount(Long siteCount) {
        this.siteCount = siteCount;
    }

    public BigDecimal getReach() {
        return reach;
    }

    public void setReach(BigDecimal reach) {
        this.reach = reach;
    }
}
