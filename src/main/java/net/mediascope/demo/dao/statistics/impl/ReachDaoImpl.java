package net.mediascope.demo.dao.statistics.impl;

import net.mediascope.demo.dao.statistics.ReachDao;
import net.mediascope.demo.model.statistics.SiteReachDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReachDaoImpl implements ReachDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SiteReachDetails> getAllSiteReachDetails() {
        String sql = "SELECT " +
                "R.HOLDING_ID AS holdingId, " +
                "R.SITE_ID AS siteId, " +
                "R.REACH AS reach, " +
                "HC.COUNT AS holdingCount " +
                "FROM STATISTICS.REACH AS R " +
                "LEFT JOIN STATISTICS.HOLDING_COUNTER AS HC " +
                "ON R.HOLDING_ID = HC.HOLDING_ID;";
        return jdbcTemplate.query(sql, (rs, rowNum) -> {
            SiteReachDetails details = new SiteReachDetails();
            details.setHoldingId(rs.getLong("holdingId"));
            details.setSiteId(rs.getLong("siteId"));
            details.setReach(rs.getBigDecimal("reach"));
            details.setHoldingCount(rs.getLong("holdingCount"));
            return details;
        });
    }
}
