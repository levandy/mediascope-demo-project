package net.mediascope.demo.dao.statistics;

import net.mediascope.demo.model.statistics.SiteReachDetails;

import java.util.List;

public interface ReachDao {

    List<SiteReachDetails> getAllSiteReachDetails();

}
