package net.mediascope.demo.util;

import java.math.BigDecimal;

import static java.math.BigDecimal.ROUND_HALF_DOWN;

public class StatisticsCalculator {

    public static Long calculateSiteCount(BigDecimal reach, Long holdingCount) {
        return reach
                .divide(new BigDecimal(100), reach.scale() + 2, ROUND_HALF_DOWN)
                .multiply(new BigDecimal(holdingCount))
                .longValue();
    }
}
