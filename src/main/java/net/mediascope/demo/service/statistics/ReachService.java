package net.mediascope.demo.service.statistics;

import net.mediascope.demo.dto.statistics.SiteReachDto;

import java.util.List;

public interface ReachService {

    List<SiteReachDto> getDetails();
}
