package net.mediascope.demo.service.statistics.impl;

import net.mediascope.demo.dao.statistics.ReachDao;
import net.mediascope.demo.dto.statistics.SiteReachDto;
import net.mediascope.demo.model.statistics.SiteReachDetails;
import net.mediascope.demo.service.statistics.ReachService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingLong;
import static java.util.stream.Collectors.toList;
import static net.mediascope.demo.util.StatisticsCalculator.calculateSiteCount;
import static org.springframework.util.CollectionUtils.isEmpty;

@Service
public class ReachServiceImpl implements ReachService {

    private ReachDao reachDao;

    @Autowired
    public ReachServiceImpl(ReachDao reachDao) {
        this.reachDao = reachDao;
    }

    @Override
    public List<SiteReachDto> getDetails() {
        List<SiteReachDetails> detailsList = reachDao.getAllSiteReachDetails();
        if (isEmpty(detailsList)) {
            return emptyList();
        }
        detailsList.forEach(detail -> detail.setSiteCount(
                calculateSiteCount(detail.getReach(), detail.getHoldingCount())
        ));
        return detailsList.stream()
                .collect(groupingBy(SiteReachDetails::getSiteId, summingLong(SiteReachDetails::getSiteCount)))
                .entrySet()
                .stream()
                .map(entry -> new SiteReachDto(entry.getKey(), entry.getValue()))
                .collect(toList());
    }
}
