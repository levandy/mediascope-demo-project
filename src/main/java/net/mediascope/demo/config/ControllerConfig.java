package net.mediascope.demo.config;

import net.mediascope.demo.controller.actuator.HealthController;
import net.mediascope.demo.controller.statistics.ReachController;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ControllerConfig extends ResourceConfig {

    public ControllerConfig() {
        this.initEndpoints();
    }

    private void initEndpoints() {
        register(ReachController.class);
        register(HealthController.class);
    }
}
