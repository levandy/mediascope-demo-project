package net.mediascope.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;

import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;

@Configuration
public class DataSourceConfig {

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder()
                .setType(H2)
                .setName("testdb")
                .setScriptEncoding("UTF-8")
                .addScript("classpath:sql/create-schemas.sql")
                .addScript("classpath:sql/create-statistics-tables.sql")
                .addScript("classpath:sql/insert-statistics-data.sql")
                .build();
    }
}
